function Node(char) {// Defining the node
	this.char = char;
	this.isWord = false;
	this.children = {};
}

function TrieTree() {
	this.root = new Node('');	//head of the tree
}

TrieTree.prototype.add = function(word) {
	if(!this.root) {
    		return null;
 	 }
 	 
  	this.addNode(this.root, word);
};

TrieTree.prototype.addNode = function(node, word) {
	if(!node || !word) {//check if the node or word exist
    		return null;
  	}
  	var character = word.charAt(0);//get the character in the word
  	var child = node.children[character];
  	
  	if(!child) {//put the new Node to the node
  		child = new Node(character);
   		node.children[character] = child;
  	}
  	
  	var remainder = word.substring(1);//check if the character is the last character in the word
  	if(!remainder||remainder==('\n')) {
    		child.isWord = true;
  	}
  	
  	this.addNode(child, remainder);
};

TrieTree.prototype.exist = function(word) {
	if(!this.root) {	//check if the tree is empty
    		return false;
  	}
  	return this.existWord(this.root, word);
};

TrieTree.prototype.existWord = function(node, word) {
	if(!node || !word ) {
    		return false;
  	}
  	var character = word.charAt(0);
  	var child = node.children[character];
  	if(child) {
	    	var remainder = word.substring(1);
		if(!remainder && child.isWord) {//check if the last character completes a word
		    return true;
		} else {
		    return this.existWord(child, remainder);
	}
	} else {
	    return null;
	}
};

var dict = new TrieTree();
//Reading Dictionary
var readDictionary = function(event) {
        var input = event.target;
	var path = document.location.pathname;
	var currentDirectory = path.substring(path.indexOf('/'), path.lastIndexOf('/'));
	var dictFile = currentDirectory.concat("/");
	    dictFile = dictFile.concat("TWL06.txt");
	console.log(dictFile);
        var reader = new FileReader();
        reader.onload = function(dictFile){
		 var lines = reader.result;
		 var lines = this.result.split('\n');
		 for(var line = 0; line < lines.length; line++){
		    var word = lines[line].substring(0,lines[line].length-1);
		    if(word.length>=3)  dict.add(word);//delete the last character in a line
		 }
		 console.log(dict);
		  };
          reader.readAsText(input.files[0]);
};

function checkIfAdj(r,c, row, col, traySize, tray){
	if((r>=0 && r<traySize) && (c>=0 && c<traySize)){
		// check if adjacent
		if(((row-1)==r && col==c) || ((row+1)==r && col==c) || (row==r && (col-1)==c) || (row==r && (col+1)==c) || ((row-1)==r && (col-1)==c) || ((row-1)==r && (col+1)==c) || ((row+1)==r && (col-1)==c) || ((row+1)==r && (col+1)==c)) return 1; // above right
	}
	return 0;
}

function backtracking(traySize,tray){
	var start, move, N=traySize*traySize;
	var nopts = new Array(N+2); //array top of stacks
	var option = new Array(N+2);
	for (var a=0; a <(N+2); a++) //array stacks of options
		    option[a]=new Array(N+2);
	var i, candidate, sum=0;
	var row, col, k=0;
	var word=[];
	var tempW = new Array(N+1);
	
	var nSolutions = 0;
	var listOfWord = [];
	console.log("\nSolutions:\n");
	var text = document.getElementById('output').innerText
	document.getElementById('output').innerText = text+"\n SOLUTIONS";
	
	// for debugging
	var num=0;
	var start = performance.now();

	move = start = 0; 
	nopts[start]= 1;
	
	while (nopts[start] >0)
	{
		if(nopts[move]>0)
		{
			move++;
			nopts[move]=0; //initialize new move

			word= [];
			for(i=1,k=0;i<move;i++,k++){
				word[k]=tray[Math.floor(option[i][nopts[i]]/traySize)][(option[i][nopts[i]]%traySize)];
			}
			words=word.join("");//convert the array to string
			
			//console.log("words "+words);

			if(dict.exist(words)){
				if(listOfWord.indexOf(words)==-1){
					//console.log(words);
					var text = document.getElementById('output').innerText
					document.getElementById('output').innerText = text+"\n"+words;
					listOfWord.push(words);
					nSolutions++;
				}
			}
			//populate
			//find candidates
			for(row = 0; row<traySize; row++){
				for(col = 0; col<traySize; col++){
					word= [];
					for(i=1,k=0;i<move;i++,k++){
						word[k]=tray[Math.floor(option[i][nopts[i]]/traySize)][(option[i][nopts[i]]%traySize)];
					}
					words=word.join("");//convert the array to string

					if(word && dict.exist(words)==null){
						//console.log("words "+words);
						break;
					}

					var rw = Math.floor(option[move-1][nopts[move-1]]/traySize);
					var cl = (option[move-1][nopts[move-1]]%traySize);

					for (i = move-1,k=0; i>=1; i--,k++){ // check for repetition
						if(option[i][nopts[i]]==((row*traySize)+col)) break;
					}

					if(move==1 || (i==0 && checkIfAdj(rw, cl, row, col, traySize, tray)==1)) option[move][++nopts[move]] = ((row*traySize)+col);
				}
			}
		}
		else 
		{
			//backtrack
			move--;
			nopts[move]--;
		}
	}
	/* check if no solution */
	if(nSolutions==0){
		console.log("No Solution!\n");
		var text = document.getElementById('output').innerText
		document.getElementById('output').innerText = text+"\n No Solutions!!";
	}else{
		console.log("Solutions: "+nSolutions);
	}

	var end = performance.now();
	var time = end - start;
	var minutes = Math.floor(time / 60000);
  	var seconds = ((time % 60000) / 1000).toFixed(0);
	console.log('Execution time: ' + minutes + ":" + (seconds < 10 ? '0' : '') + seconds);
}
        
var openFile = function(event) {
    var input = event.target;
    var reader = new FileReader();
    reader.onload = function(){
        var lines = reader.result;
        var lines = this.result.split('\n');
        var numTray = lines[0];
        var listOfTrays = new Array();
        var lineNum =1;
	    for(var i=0;i<numTray;i++){
	    	var dimensionRow = lines[lineNum++];
	    	var tray =new Array(dimensionRow);
		    for(var row = 0; row < dimensionRow; row++){
		      	var lineLetters = lines[lineNum].split(' ');
		      	for(var col = 0; col < dimensionRow; col++){
		      		if(col==0) tray[row] = new Array(dimensionRow);
		      		var text = document.getElementById('input').innerText;
			  		document.getElementById('input').innerText = text+lineLetters[col]+" ";
		      		tray[row][col]=lineLetters[col][0];
		      	}
		      	lineNum++;
		    }
		    backtracking(dimensionRow, tray);
		    //console.log(checkIfAdj(0,1, 2,2, dimensionRow, tray));
			listOfTrays[i]=tray;
	    } 
    };
    reader.readAsText(input.files[0]);
};
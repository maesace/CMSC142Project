#include <stdio.h>
#include <stdlib.h>
#define N 5

int main(){
	int start, move;
	int nopts[N+2]; //array top of stacks
	int option[N+2][N+2]; //array stacks of options
	int i, candidate, sum=0;

	move = start = 0; 
	nopts[start]= 1;
	
	while (nopts[start] >0)
	{
		if(nopts[move]>0)
		{
			move++;
			nopts[move]=0; //initialize new move

			sum=0;
			for (i = move-1; i>=1; i--) { // check sum
				sum+=option[i][nopts[i]];
			}
			//print - solution found!
			if(sum==N){
				for(i=1;i<move;i++)
					printf("%2i",option[i][nopts[i]]);
				printf("\n");
			}
			//populate
			else //find candidates
			{
				for(candidate = N; candidate >=1; candidate --) 
				{
					sum = 0;
					for (i = move-1; i>=1; i--){ // check sum
						sum+=option[i][nopts[i]];
					}
					if((sum+candidate)<=N){ // add candidate if within sum
						option[move][++nopts[move]] = candidate;
					}
				}
			}
		}
		else 
		{
			//backtrack
			move--;
			nopts[move]--;
		}
	}

}

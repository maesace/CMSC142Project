#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct node{
	char x;
	struct node *next;
}graph;

int checkIfWord(char* word){
	FILE *file = fopen ( "TWL06.txt", "r" );
	if ( file == NULL ){
		perror ( "TWL06.txt" );
		return;
	}
	
	char line [ 128 ]; /* or other suitable maximum line size */
	int i;
	while (!feof(file)) {
	fscanf (file, "%s\n", line);
	//printf("-%s-\n", line);
		if(strlen(line)>=3 && strcmp(word, line)==0){
			return 1;
		}
	}
	fclose ( file );
}

graph* newNode(char x){
	graph *newNode = (graph *)malloc(sizeof(graph));
	newNode->x = x;
	newNode->next = NULL;
	return newNode;
}

void viewList(graph **g, int traySize){
	int i;
	graph *p;	

	for(i = 0; i < (traySize*traySize); i++){
		p = g[i];
		printf("%d: ", i);
		//traverse the list connected to vertex i
		while(p != NULL){
			printf("%c ", p->x);
			p = p->next;
		}
		printf("\n");
	}
}

int checkIfAdj(graph **g, char c, char adj, int traySize){
	int i;
	graph *p;	

	for(i = 0; i < (traySize*traySize); i++){
		// find vertex
		p = g[i];
		if(p->x == c){ // vertex found
			p = p->next;
			while(p != NULL){
				if(p->x == adj) break;
				p = p->next;
			}
			if(p==NULL) return 0;
			return 1;
		}
	}
}

graph **createAdjList(int traySize, char tray[traySize][traySize]){
		graph**	g = (graph **)malloc(sizeof(graph*)*(traySize*traySize));

		int a,b,c,k = 0;
		for(a=0; a<traySize; a++){
			for(b=0; b<traySize; b++){
				// initialize
				g[k++] = newNode(tray[a][b]);
			}
		}
		// loop through tray
		// insert adjacent
		graph *new;
		k = 0;
		for(a=0; a<traySize; a++){
			for(b=0; b<traySize; b++){
				// insert adjacent
				if((a-1)>=0){// adj above
					new = newNode(tray[a-1][b]);
					new->next = g[k]->next;
					g[k]->next = new;
				}
				
				if((a+1)<traySize){// adj below
					new = newNode(tray[a+1][b]);
					new->next = g[k]->next;
					g[k]->next = new;
				}
				
				if((b-1)>=0){// adj left
					new = newNode(tray[a][b-1]);
					new->next = g[k]->next;
					g[k]->next = new;
				}
				
				if((b+1)<traySize){// adj right
					new = newNode(tray[a][b+1]);
					new->next = g[k]->next;
					g[k]->next = new;
				}
				
				if((a-1)>=0 && (b-1)>=0){// adj diagonal above left
					new = newNode(tray[a-1][b-1]);
					new->next = g[k]->next;
					g[k]->next = new;
				}
				
				if((a-1)>=0 && (b+1)<traySize){// adj diagonal above right
					new = newNode(tray[a-1][b+1]);
					new->next = g[k]->next;
					g[k]->next = new;
				}
				
				if((a+1)<traySize && (b-1)>=0){// adj diagonal below left
					new = newNode(tray[a+1][b-1]);
					new->next = g[k]->next;
					g[k]->next = new;
				}
				
				if((a+1)<traySize && (b+1)<traySize){// adj diagonal below right
					new = newNode(tray[a+1][b+1]);
					new->next = g[k]->next;
					g[k]->next = new;
				}
				
				k++;
			}
		}
	return g;
}

/*
const char* traceWord(graph **g, char c, int traySize){
	int i,j=0;
	graph *p;	
	char word[traySize*traySize+1];
	strcpy(word,"");


printf("---------------------------\n");
	word[j++] = c;
	while(p!=NULL){
		// find vertex
		printf("find vertex %s\n",word);
		p = g[i++];
		if(p->x == word[j-1]){ // vertex found
			p = p->next;
			/*
			while(p != NULL){
			 if (!strchr(word, p->x)){// char does not exist in word
			 	word[j++] = p->x;
					c = p->x;
					i = 0;
			 	break;
			 }
			 printf("char %c\n", p->x);
				p = p->next;
			}/
					 printf("char %c\n", p->x);
			 	word[j++] = p->x;
			 	i = 0;
		}
		word[j] = '\0';
printf("---------------------------\n");
		return word;
	}
}
*/

void backtracking(int traySize, char tray[traySize][traySize]){
	int start, move, N=traySize*traySize;
	int nopts[N+2]; //array top of stacks
	char option[N+2][N+2]; //array stacks of options
	int i, candidate, sum=0;

	move = start = 0; 
	nopts[start]= 1;
	
	while (nopts[start] >0)
	{
		if(nopts[move]>0)
		{
			move++;
			nopts[move]=0; //initialize new move

			sum=0;
			for (i = move-1; i>=1; i--) { // check sum
				sum+=option[i][nopts[i]];
			}
			//print - solution found!
			if(sum==N){
				for(i=1;i<move;i++)
					printf("%2i",option[i][nopts[i]]);
				printf("\n");
			}
			//populate
			else //find candidates
			{
				for(candidate = N; candidate >=1; candidate --) 
				{
					sum = 0;
					for (i = move-1; i>=1; i--){ // check sum
						sum+=option[i][nopts[i]];
					}
					if((sum+candidate)<=N){ // add candidate if within sum
						option[move][++nopts[move]] = candidate;
					}
				}
			}
		}
		else 
		{
			//backtrack
			move--;
			nopts[move]--;
		}
	}
}

main(){
	graph **g;

	FILE *file = fopen ( "input.in", "r" );
	if ( file == NULL ){
		perror ( "input.in" );
		return;
	}

	/* read file */
	char line [ 128 ];
	int numOfBoggles, boggles, a, b, traySize;
	fscanf (file, "%d\n", &numOfBoggles);// get number of boggles
//	for(boggles=0; boggles<numOfBoggles; boggles++){
		fscanf (file, "%d\n", &traySize);// get tray size
		char tray[traySize][traySize]; // create tray
		// get characters in tray
		for(a=0; a<traySize; a++){
			for(b=0; b<traySize; b++){
				tray[a][b] = fgetc (file);fgetc (file);
			}tray[a][b] = '\0';
		}
		/* do word search here*/
		/* print tray */
		printf("Print Tray:\n");
		for(a=0; a<traySize; a++){
			for(b=0; b<traySize; b++){
				printf("%c ", tray[a][b]);
			}printf("\n");
		}
		
		/* create adjacent list */
		g = createAdjList(traySize, tray);
		
		/* view adjacent list */
		viewList(g, traySize);
		
		/* get all possible combinations */
		backtracking(traySize, tray);
		
//	}
	fclose ( file );
	
	
	//if(checkIfWord("HERE")==1) printf("found\n");
	//if(checkIfWord("FAIL")==1) printf("found\n");
	
	//if(checkIfAdj(g,'A','D',4)==1)printf("adjacent\n");
	//else printf("not");
}

/*
	Exercise: Vertex Coloring

	1. Complete the basic greedy coloring program below.

	2. Analyze the running time of the program. Use |V| or |E| when specifying order of growth

	3. Determine a good ordering of vertices that will give the optimal solution. (for the given graph)

	4. Give the chromatic number of the graph

	

	Program Usage: ./exeName fileName

	Example: ./graphColoring graph.in



	Vertex Coloring Greedy Algorithm:

		Consider the vertices in a specific order:

		v1, v2, …, vn.



		Color the first vertex with first color.



		For the other vertices:

			For each vertices already colored and adjacent to the current vertex:

				identify the lowest color not used

				if a color is identified:

					color the current vertex with this

				else:

					create a new color and use this to the current vertex



	Answers for numbers 2 to 4:

		2. O(|V|+|E|) because the algorithm checks the edges of each vertex

		3. 6,5,4,3,2,1,0

		4. 4 (or 2 in optimal solution)



*/



#include <stdio.h>

#include <malloc.h>

#include <stdlib.h>

#define UNDIRECTED 0

#define	DIRECTED 1



typedef struct node{

	int x;

	struct node *next;

}graph;



graph **createAdjList(char *, int *, int *, int *);

void viewList(graph **, int);

int * graphColoring(graph **, int);

int getAvailableColor(graph **, int *, int, int);

void viewColor(int *, int);

void deleteGraph(graph **, int);



main(int argc, char *argv[]){

	graph **g;

	int v, e, type;

	int *color;

	char *fileName = argv[1];

	

	g = createAdjList(fileName, &v, &e, &type);

	color = graphColoring(g, v);

	viewList(g, v);

	viewColor(color, v);



	free(color);

	deleteGraph(g, v);

}



graph* newNode(int x){

	graph *newNode = (graph *)malloc(sizeof(graph));

	newNode->x = x;

	newNode->next = NULL;

	return newNode;

}



graph **createAdjList(char *fileName, int *v, int *e, int *type){

	/*insert code here for creating an adjacency list from the input file named fileName*/



	// declare adjacent list

	graph **g = NULL;



	FILE *file = fopen ( fileName, "r" );

	if(file != NULL) {

	/* read line */

		char line [128];

		// 1st line num of vertices

		(*v) = fgets(line, sizeof line, file)[0]-'0';

		// 2nd line num of edges

		(*e) = fgets(line, sizeof line, file)[0]-'0';

		// 3rd line type of graph (0=undirected; 1=directed)

		(*type) = fgets(line, sizeof line, file)[0]-'0';



		// initialize adjacent list

		g = (graph **)malloc(sizeof(graph*)*(*v));

		int i;

		for(i=0; i<(*v); i++) g[i] = NULL;



		char* temp;

		int x,adjToX;

		graph *tempNode, *new;

		while ( (temp = fgets(line, sizeof line, file)) != NULL ){

			x = temp[0]-'0';

			adjToX = temp[2]-'0';



			// insert at head of list

			if(g[x]==NULL){ // if list is empty

				g[x] = newNode(adjToX); // place at start

			}else{ // if list is not empty

				new = newNode(adjToX);

				new->next = g[x];

				g[x] = new;

			}

			// do it for other vertex if undirected

			if((*type)==UNDIRECTED){

				if(g[adjToX]==NULL){ // if list is empty

					g[adjToX] = newNode(x); // place at start

				}else{ // if list is not empty

					new = newNode(x);

					new->next = g[adjToX];

					g[adjToX] = new;

				}

			}

		}

		fclose(file);

	}

	else{

	  	printf("File not found.");

	}

	// return adjacent list

	return g;

}



void viewList(graph **g, int v){

	int i;

	graph *p;

	

	for(i = 0; i < v; i++){

		p = g[i];

		printf("%d: ", i);

		//traverse the list connected to vertex i

		while(p != NULL){

			printf("%3d", p->x);

			p = p->next;

		}

		printf("\n");

	}

}



int * graphColoring(graph **g, int v){

	int *color, availCol, i;

//	int sequence[] = {0,1,2,3,4,5,6};

	int sequence[] = {6,5,4,3,2,1,0};

	

	//colors are represented as integers starting from 0

	color = (int *)malloc(sizeof(int)*v);

	for(i = 0; i < v; i++)

		color[i] = -1;

	

	color[sequence[0]] = 0;	//assign first vertex with the first color

	for(i = 1; i < v; i++){

		availCol = getAvailableColor(g, color, v, sequence[i]);

		color[sequence[i]] = availCol;

	}



	return color;

}



int getAvailableColor(graph **g, int *color, int v, int curr){

	graph *p;

	int *available, i, availCol;

	

	//keeps track of the colors used on any previously colored vertices adjacent to it

	available = (int *)malloc(sizeof(int)*(v));

	for(i = 0; i < v; i++)

		available[i] = 1;



	

	/*Insert code here for marking the colors that have been used 

	on any previously colored vertices adjacent to it.*/

	p = g[curr]; // get current vertex

	while(p != NULL){

        if(color[p->x]!=-1){// color is used 

        	available[color[p->x]] = 0;

        }

        p = p->next;

    }

	

	

	for(i = 0; i < v; i++){				//get the smallest color that is available

		if(available[i] == 1){

			availCol = i;

			break;

		}

	}

	

	free(available);

	return availCol;

}



void viewColor(int *color, int v){

	int i;

	

	for(i = 0; i < v; i++){

		printf("Vertex %d -> Color %d\n", i, color[i]);

	}

}



void deleteGraph(graph **g, int v){

	int i;

	graph *p;

	for(i = 0; i < v; i++){

		while(g[i] != NULL){

			p = g[i];

			g[i] = g[i]->next;

			free(p);

		}

	}

	free(g);

}
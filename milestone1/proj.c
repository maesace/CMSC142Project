/*

  Milestone 1
  Authors:
    Felicia Mae L. Sace
    Kristel Diane Artificio
    Ariel Doria

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char** getDictionary(int *k){
	char **dictionary = malloc(999999 * sizeof(char *));
	FILE *file = fopen ( "TWL06.txt", "r" );
	if ( file == NULL ){
		perror ( "TWL06.txt" );
		return;
	}
	
	char word[ 128 ];
	int i;
	while (!feof(file)) {
		fscanf (file, "%s\n", word);
		if(strlen(word)>=3){
		  dictionary[(*k)] = malloc(strlen(word)+1);
		  strncpy(dictionary[(*k)++], word, strlen(word));
		}
	}
	fclose ( file );
	
	
	return dictionary;
}

int checkIfWord(char **dictionary, int nWords, char* word){	
	int i;
	if(nWords==0) return -1;
	for(i=0; i<nWords; i++){
		if(strcmp(dictionary[i], word)==0) return 1;
	}
	return 0;
}

int checkIfAdj(char c, char adj, int traySize, char tray[traySize][traySize]){
	if(c=='\0') return 1;// because empty char
	int row,col;
	for(row=0; row<traySize; row++){
		for(col=0; col<traySize; col++){
			if(tray[row][col]==c){
				// check if adjacent
				if((row-1)>=0 && tray[row-1][col] == adj) return 1;
				if((row+1)<traySize && tray[row+1][col] == adj) return 1;
				if((col-1)>=0 && tray[row][col-1] == adj) return 1;
				if((col+1)<traySize && tray[row][col+1] == adj) return 1;
				if((row-1)>=0 && (col-1)>=0 && tray[row-1][col-1] == adj) return 1;
				if((row-1)>=0 && (col+1)<traySize && tray[row-1][col+1] == adj) return 1;
				if((row+1)<traySize && (col-1)>=0 && tray[row+1][col-1] == adj) return 1;
				if((row+1)<traySize && (col+1)<traySize && tray[row+1][col+1] == adj) return 1;			
				break;
			}
		}
	}
	return 0;
}

void backtracking(int traySize, char tray[traySize][traySize], int nWords, char **dictionary){

	/* set up write in file */	
	FILE *file = fopen ( "output.txt", "w+" );
	if ( file == NULL ){
		perror ( "output.txt" );
		return;
	}

	int start, move, N=traySize*traySize;
	int nopts[N+2]; //array top of stacks
	char option[N+2][N+2]; //array stacks of options
	int i, candidate, sum=0;
	int row, col, k=0;
	char word[N+1], tempW[N+1];
	
	int nSolutions = 0;
		
	for(row = 0; row<traySize; row++){
		for(col = 0; col<traySize; col++){
			fprintf(file, "%c ", tray[row][col]);
		}fprintf(file, "\n");
	}
	fprintf(file, "\n\n");			
				
		
	printf("\nSolutions:\n");
	fprintf(file, "Solutions:\n");
	
	move = start = 0; 
	nopts[start]= 1;
	
	while (nopts[start] >0)
	{
		if(nopts[move]>0)
		{
			move++;
			nopts[move]=0; //initialize new move

			
			strcpy(word,"");
			for(i=1,k=0;i<move;i++,k++){
				word[k] = option[i][nopts[i]];
			}word[k] = '\0';


			//print - solution found!
			if(checkIfWord(dictionary, nWords, word)==1){
				fprintf(file, "%s\n", word);
			  printf("%s\n", word);
			  nSolutions++;
			}
			//populate
			else //find candidates
			{
				for(row = 0; row<traySize; row++){
					for(col = 0; col<traySize; col++){
						strcpy(word,"");
						for (i = move-1,k=0; i>=1; i--,k++){ // check for repetition
							if(option[i][nopts[i]]==tray[row][col]) break;
							word[k++] = option[i][nopts[i]];
						}word[k]='\0';

						if(i==0){
							if(checkIfAdj(option[move-1][nopts[move-1]], tray[row][col], traySize, tray)==1)
								option[move][++nopts[move]] = tray[row][col];
						}
					}
				}
			}
		}
		else 
		{
			//backtrack
			move--;
			nopts[move]--;
		}
	}
	
	/* check if no solution */
	if(nSolutions==0) printf("No Solution!\n");
	
	fclose ( file );
}

main(){
	int nWords = 0;
	char **dictionary = getDictionary(&nWords);
	
	FILE *file = fopen ( "input.in", "r" );
	if ( file == NULL ){
		perror ( "input.in" );
		return;
	}

	/* read file */
	char line [ 128 ];
	int numOfBoggles, boggles, a, b, traySize;
	fscanf (file, "%d\n", &numOfBoggles);// get number of boggles
	for(boggles=0; boggles<numOfBoggles; boggles++){
		fscanf (file, "%d\n", &traySize);// get tray size
		char tray[traySize][traySize]; // create tray
		// get characters in tray
		for(a=0; a<traySize; a++){
			for(b=0; b<traySize; b++){
				tray[a][b] = fgetc (file);fgetc (file);
			}tray[a][b] = '\0';
		}
		/* do word search here*/
		/* print tray */
		printf("Print Tray:\n");
		for(a=0; a<traySize; a++){
			for(b=0; b<traySize; b++){
				printf("%c ", tray[a][b]);
			}printf("\n");
		}
		
		/* get all possible combinations */
		backtracking(traySize, tray,nWords,dictionary);
		
	}
	fclose ( file );
}
